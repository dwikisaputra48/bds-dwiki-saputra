<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class RumahSakitModel extends Model
{
    public static function rumahSakitList()
    {
        $query = DB::table('rumah_sakit')
                    ->select([
                        'rumah_sakit.*', 
                        'kelurahan.nama_kota', 'kelurahan.nama_kecamatan', 'kelurahan.nama_kelurahan'
                    ])
                    ->join('kelurahan', 'rumah_sakit.kode_kelurahan', '=', 'kelurahan.kode_kelurahan')
                    ->get();

        $result = [];
        if(count($query)) $result = collect($query)->map(function($x){ return (array) $x; })->toArray();

        return $result;


    }
}
