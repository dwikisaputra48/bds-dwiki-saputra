<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

use App\Models\RumahSakitModel as RumahSakitM;

class MainController extends Controller
{

    public function __construct(){

    }

    public function getRs()
    {
        $getData = RumahSakitM::rumahSakitList();
        // dd($getData);
        // $getData = [];

        $response['status'] = 'failed'; 
        $response['count'] = 0; 
        $response['data'] = []; 
        if(count($getData) > 0){
            $response['status'] = 'success'; 
            $response['count'] = count($getData); 
            foreach($getData as $kData => $vData){
                $response['data'][$kData]['id'] = $vData['id'];
                $response['data'][$kData]['nama_rsu'] = $vData['nama_rsu'];
                $response['data'][$kData]['jenis_rsu'] = $vData['jenis_rsu'];
                $response['data'][$kData]['location'] = [
                    'latitude' => floatval($vData['latitude']),
                    'longitude' => floatval($vData['longitude'])
                ];
                $response['data'][$kData]['alamat'] = $vData['alamat'];
                $response['data'][$kData]['kode_pos'] = floatval($vData['kode_pos']);
                $response['data'][$kData]['telepon'] = json_decode($vData['telepon'], TRUE);
                $response['data'][$kData]['faximile'] = json_decode($vData['faximile'], TRUE);
                $response['data'][$kData]['website'] = $vData['website'];
                $response['data'][$kData]['email'] = implode(json_decode($vData['email'], TRUE), ', ');
                $response['data'][$kData]['kelurahan'] = [
                    'kode' => (string)$vData['kode_kelurahan'],
                    'nama' => $vData['nama_kelurahan']
                ];
                $response['data'][$kData]['kecamatan'] = [
                    'kode' => (string)$vData['kode_kecamatan'],
                    'nama' => $vData['nama_kecamatan']
                ];
                $response['data'][$kData]['kota'] = [
                    'kode' => (string)$vData['kode_kota'],
                    'nama' => $vData['nama_kota']
                ];
            }
        }

        return (new Response($response, 200))
            ->header('Content-Type', 'application/json');
    }

    
}
